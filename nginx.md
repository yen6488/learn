# nginx
防止.git 或其它 . 開頭的資料夾曝露在網路上.

    location ~ /\.
    {
        deny all;
    }
